## Описание метода
Возвращает полную информацию о договоре и его текущем статусе лота

## Запрос
Action: `Contract` 
Method: `load`

* `data` 
    * `lot_id` - Номер лота по которому загружается договор 

### Пример запроса
```json
{
  "action": "Contract",
  "method": "load",
  "data": [
    {
      "lot_id": "1234"
    }
  ]
}
```

## Ответ
* `result` 
    * `lotInfo` - Краткая информация о лот договора 
        * `id` - ID лота 
        * `procedure_id` - ID процедуры 
        * `procedure_type` - тип процедуры
        * `status` - статус процедуры
        * `registry_number` - рег. номер процедуры
        * `number` - номер лота
        * `title` - наименование лота
        * `price` - цена лота
        * `final_price` - финальная цена лота
        * `winner_id` - ID победителя
        * `contract_supplier_id` - ID поставщика с которым заключается контракт
    * `suppliers` - 
    * `customers` - Заказчики по контракту (массив) 
        * `id` - ID заказчика
        * `full_name` - полное наименование заказчика
        * `short_name` - краткое наименование заказчика
    * `organizerContragent` - Информация об организаторе 
    * `currentSupplier` - Текущий поставщик (среди победителей)
    * `currentSupplierInfo` - Информация о текущем поставщике
        * `full_name` - полное наименование
        * `short_name` - краткое наименование
        * `inn` - ИНН 
        * `kpp` - КПП
        * `okpo` - ОКПО
        * `okopf` - ОКОПФ
        * `address_main` - Юр. адрес 
        * `address_post` - Почтовый адрес 
        * `phone` - Телефон 
        * `email` - Email
        * `fax` - Факс
        * `bank` - Банк 
        * `bik` - БИК
        * `bank_addr` - Адрес банка
        * `account_ras` - Расчетный счет 
        * `account_cor` - Кор. счет
        * `account_lic` - Лицевой счет
        * `small_biz` - Признак МСП 
    * `files` - Файлы договора (массив, включая доп. файлы которые крепятся к договору) 
        * `id` - ID файла
        * `date_added` - дата добавления
        * `is_last_version` - признак последней версии
        * `type_id` - тип документа договора
        * `filename` - имя файла
        * `hash` - хеш 
        * `size` - размер
        * `supplier_id` - поставщик по договору
        * `customer_id` - заказчик по договору
        * `date_supplier_eds` - дата подписания поставщиком 
        * `date_customer_eds` - дата подписания заказчиком
        * `oos_id` - ID файла в ЕИС
        * `oos_registry_number` - Рег. номер в ЕИС
        * `oos_version` - Версия документа в ЕИС
        * `oos_publish_status` - Статус публикации в ЕИС 
        * `name` - Имя файла
        * `link` - Ссылка на файл 
        * `description` - Описание файла
        * `signatures` - Подписи 
            * `customer` - Заказчика
            * `supplier` - Поставщика
    * `currency` - Валюта договора
        * `name` - Название валюты 
        * `description` - Описание
    * `currentContract` - Информация о договоре 
        * `is_last_version` - признак последней версии
        * `type_id` - тип документа договора 
        * `filename` - имя файла
        * `hash` - хеш
        * `size` - размер файла
        * `supplier_id` - поставщик по договору
        * `customer_id` - заказчик по договору
        * `date_supplier_eds` - дата подписания поставщиком 
        * `date_customer_eds` - дата подписания заказчиком
        * `oos_id` - ID файла в ЕИС
        * `oos_registry_number` - Рег. номер в ЕИС
        * `oos_version` - Версия документа в ЕИС
        * `oos_publish_status` - Статус публикации в ЕИС 
        * `name` - Имя файла
        * `link` - Ссылка на файл 
        * `description` - Описание файла
        * `supplierSigned` - подписал ли поставщик 
        * `customerSigned` - подписал ли заказчик 
        * `signatures` - подписи
            * `customer` - заказчика
            * `supplier` - поставщика
        * `oos_contract_info` - информация о договоре в ЕИС 
            * `type` - тип договора в ЕИС
            * `oos_contract_id` - GUID договора в ЕИС
            * `version` - версия
            * `date_published` - дата публикации 
            * `status` - статус
            * `date_send` - дата отправки
            * `error_detail` - информация об ошибке

### Пример ответа
```json
{
  "action": "Contract",
  "method": "load",
  "tid": 19,
  "type": "rpc",
  "result": {
    "mode": "View",
    "dual_mode": false,
    "mode_additional": false,
    "lotInfo": {
      "id": 2365,
      "procedure_id": 2467,
      "procedure_type": 10,
      "status": 8,
      "registry_number": "AVT11022000001",
      "number": 1,
      "title": "\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0435 \u0437\u0435\u043c\u0435\u043b\u044c\u043d\u043e-\u043a\u0430\u0434\u0430\u0441\u0442\u0440\u043e\u0432\u044b\u0445 \u0440\u0430\u0431\u043e\u0442 \u043f\u043e \u0434\u043e\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u043c\u0443 \u043e\u0442\u0432\u043e\u0434\u0443 \u0432 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0438 \u0437\u0435\u043c\u0435\u043b\u044c\u043d\u044b\u0445 \u0443\u0447\u0430\u0441\u0442\u043a\u043e\u0432 \u043d\u0430 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044c\u043d\u043e\u0439 \u0434\u043e\u0440\u043e\u0433\u0435 \u0410-107 \u00ab\u041c\u043e\u0441\u043a\u043e\u0432\u0441\u043a\u043e\u0435 \u043c\u0430\u043b\u043e\u0435 \u043a\u043e\u043b\u044c\u0446\u043e\u00bb \u0418\u043a\u0448\u0430 - \u041d\u043e\u0433\u0438\u043d\u0441\u043a - \u0411\u0440\u043e\u043d\u043d\u0438\u0446\u044b - \u0413\u043e\u043b\u0438\u0446\u044b\u043d\u043e - \u0418\u0441\u0442\u0440\u0430 - \u0418\u043a\u0448\u0430. \u0423\u0447\u0430\u0441\u0442\u043a\u0438 \u043a\u043c 0+000 - \u043a\u043c 3+500, \u043a\u043c 3+500 - \u043a\u043c 11+500, \u043a\u043c 12+500 - 16+060 \u041c\u043e\u0436\u0430\u0439\u0441\u043a\u043e-\u0412\u043e\u043b\u043e\u043a\u043e\u043b\u0430\u043c\u0441\u043a\u043e\u0433\u043e \u0448\u043e\u0441\u0441\u0435, \u0443\u0447\u0430\u0441\u0442\u043a\u0438 \u043a\u043c 0+600 - \u043a\u043c 1+650, \u043a\u043c 1+950 - \u043a\u043c 3+400 \u041c\u0438\u043d\u0441\u043a\u043e-\u041c\u043e\u0436\u0430\u0439\u0441\u043a\u043e\u0433\u043e \u0448\u043e\u0441\u0441\u0435, \u0443\u0447\u0430\u0441\u0442\u043a\u0438 \u043a\u043c 3+650 - \u043a\u043c 5+350 \u041a\u0438\u0435\u0432\u0441\u043a\u043e-\u041c\u0438\u043d\u0441\u043a\u043e\u0433\u043e \u0448\u043e\u0441\u0441\u0435",
      "price": 2054890,
      "final_price": 2054890,
      "guarantee_needed": false,
      "winner_id": 15,
      "contract_supplier_id": 15,
      "contractAgreed": false,
      "contractSigned": false
    },
    "suppliers": [],
    "customers": [
      {
        "id": 4,
        "full_name": "\u0413\u043e\u0441\u0443\u0434\u0430\u0440\u0441\u0442\u0432\u0435\u043d\u043d\u0430\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440\"",
        "short_name": "\u0413\u041a \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440\""
      }
    ],
    "organizerContragent": 4,
    "currentSupplier": 15,
    "currentSupplierInfo": {
      "id": 15,
      "full_name": "\u041e\u0431\u0449\u0435\u0441\u0442\u0432\u043e \u0441 \u043e\u0433\u0440\u0430\u043d\u0438\u0447\u0435\u043d\u043d\u043e\u0439 \u043e\u0442\u0432\u0435\u0442\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0441\u0442\u044c\u044e \u00ab\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0418\u043d\u0436\u0438\u043d\u0438\u0440\u0438\u043d\u0433\u00bb",
      "short_name": "\u041e\u041e\u041e \u00ab\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0418\u043d\u0436\u0438\u043d\u0438\u0440\u0438\u043d\u0433\u00bb",
      "guid": "66ec7e0b-d30e-fda1-7fc1-e8a9fda753b8",
      "account": "1100000158",
      "inn": "7710946388",
      "kpp": "770701001",
      "okpo": "18104088",
      "okopf": null,
      "address_main": "109012, \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0430\u044f \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u044f (\u0420\u0424, \u0420\u043e\u0441\u0441\u0438\u044f), \u0413\u043e\u0440\u043e\u0434 \u041c\u043e\u0441\u043a\u0432\u0430 \u0441\u0442\u043e\u043b\u0438\u0446\u0430 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438 \u0433\u043e\u0440\u043e\u0434 \u0444\u0435\u0434\u0435\u0440\u0430\u043b\u044c\u043d\u043e\u0433\u043e \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u044f, \u0443\u043b. \u0421\u0442\u0440\u0430\u0441\u0442\u043d\u043e\u0439 \u0431\u0443\u043b\u044c\u0432\u0430\u0440, \u0434. 9",
      "legal_address": "109012, \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0430\u044f \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u044f (\u0420\u0424, \u0420\u043e\u0441\u0441\u0438\u044f), \u0413\u043e\u0440\u043e\u0434 \u041c\u043e\u0441\u043a\u0432\u0430 \u0441\u0442\u043e\u043b\u0438\u0446\u0430 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438 \u0433\u043e\u0440\u043e\u0434 \u0444\u0435\u0434\u0435\u0440\u0430\u043b\u044c\u043d\u043e\u0433\u043e \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u044f, \u0443\u043b. \u0421\u0442\u0440\u0430\u0441\u0442\u043d\u043e\u0439 \u0431\u0443\u043b\u044c\u0432\u0430\u0440, \u0434. 9",
      "address_post": "109012, \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0430\u044f \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u044f (\u0420\u0424, \u0420\u043e\u0441\u0441\u0438\u044f), \u0413\u043e\u0440\u043e\u0434 \u041c\u043e\u0441\u043a\u0432\u0430 \u0441\u0442\u043e\u043b\u0438\u0446\u0430 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438 \u0433\u043e\u0440\u043e\u0434 \u0444\u0435\u0434\u0435\u0440\u0430\u043b\u044c\u043d\u043e\u0433\u043e \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u044f, \u0443\u043b. \u0421\u0442\u0440\u0430\u0441\u0442\u043d\u043e\u0439 \u0431\u0443\u043b\u044c\u0432\u0430\u0440, \u0434. 9",
      "postal_address": "109012, \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0430\u044f \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u044f (\u0420\u0424, \u0420\u043e\u0441\u0441\u0438\u044f), \u0413\u043e\u0440\u043e\u0434 \u041c\u043e\u0441\u043a\u0432\u0430 \u0441\u0442\u043e\u043b\u0438\u0446\u0430 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438 \u0433\u043e\u0440\u043e\u0434 \u0444\u0435\u0434\u0435\u0440\u0430\u043b\u044c\u043d\u043e\u0433\u043e \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u044f, \u0443\u043b. \u0421\u0442\u0440\u0430\u0441\u0442\u043d\u043e\u0439 \u0431\u0443\u043b\u044c\u0432\u0430\u0440, \u0434. 9",
      "phone": "7-985-5113950-",
      "email": "kosarev.b@gmail.com",
      "fax": null,
      "bank": "\u0411\u0410\u041d\u041a \u0413\u041f\u0411 (\u0410\u041e)",
      "bik": "044525823",
      "bank_addr": "\u041c\u041e\u0421\u041a\u0412\u0410, \u0423\u041b \u041d\u0410\u041c\u0401\u0422\u041a\u0418\u041d\u0410, 16, \u041a 1",
      "account_ras": "40702810194000000904",
      "account_cor": "30101810200000000823",
      "account_lic": "",
      "supplier_profile_id": 1,
      "customer_profile_id": 1,
      "date_registered": "2016-04-04 00:00:00+03",
      "small_biz": false
    },
    "currentCustomer": 4,
    "files": [
      {
        "id": 2245,
        "date_added": "2020-02-11T12:22:30+03:00",
        "is_last_version": true,
        "type_id": 1,
        "filename": "\u0414\u043e\u0433\u043e\u0432\u043e\u0440-917.rar",
        "hash": "a8c2d50faf764720b69f69063b2ea5e2ace16ca3ab4d5c6845296c5ec7b85818",
        "size": 102600,
        "supplier_id": 15,
        "customer_id": 4,
        "user_id": 4885,
        "info": false,
        "agreed": true,
        "date_agreed": "2020-02-11T12:22:30+03:00",
        "date_supplier_eds": "2020-02-12 17:08:25+03",
        "date_customer_eds": "2020-02-12 17:14:34+03",
        "oos_id": null,
        "oos_registry_number": null,
        "oos_version": 0,
        "oos_publish_status": 0,
        "name": "\u0414\u043e\u0433\u043e\u0432\u043e\u0440-917.rar",
        "sum_info": null,
        "term_info": null,
        "in_mongo": null,
        "actual": true,
        "awaiting_suppliers": false,
        "additional_suppliers_data": null,
        "registration_number": null,
        "parent_contract_id": null,
        "link": "\/file\/get\/t\/Contract\/id\/2245\/hash\/85f1d3f29b05a2cbb02e6d284b1c1ffc",
        "description": "",
        "descr": "",
        "date_agreed_end": "2020-02-18T23:59:00+03:00",
        "supplierSigned": true,
        "mainSupplierSigned": true,
        "customerSigned": true,
        "signatures": {
          "customer": "<b>\u0412\u043b\u0430\u0434\u0435\u043b\u0435\u0446 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u0413\u043e\u0441\u0443\u0434\u0430\u0440\u0441\u0442\u0432\u0435\u043d\u043d\u0430\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440\"\n<b>\u0424\u0418\u041e:<\/b> \u0421\u043e\u043b\u043e\u0432\u044c\u0435\u0432 \u0414\u043c\u0438\u0442\u0440\u0438\u0439 \u041d\u0438\u043a\u043e\u043b\u0430\u0435\u0432\u0438\u0447\n<b>\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c:<\/b> \u0414\u0438\u0440\u0435\u043a\u0442\u043e\u0440 \u0434\u0435\u043f\u0430\u0440\u0442\u0430\u043c\u0435\u043d\u0442\u0430 \u0437\u0435\u043c\u0435\u043b\u044c\u043d\u044b\u0445 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0439 \u0438 \u0443\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f \u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e\u043c\n<b>\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f:<\/b> \u0413\u043e\u0441\u0443\u0434\u0430\u0440\u0441\u0442\u0432\u0435\u043d\u043d\u0430\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440\"\n<b>\u0418\u041d\u041d:<\/b> 7717151380\n<b>\u041a\u041f\u041f:<\/b> 770701001\n<b>\u0421\u0435\u0440\u0438\u0439\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440:<\/b> 011ec68800f6aacab64a36a250f87a353a\n<b>\u0414\u0430\u0442\u0430 \u0432\u044b\u0434\u0430\u0447\u0438:<\/b> 30-10-2019 08:07:59 UTC\n<b>\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u0435\u043d \u0434\u043e:<\/b> 30-10-2020 08:17:59 UTC\n<b>\u0418\u0437\u0434\u0430\u0442\u0435\u043b\u044c \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u0410\u041e \"\u0415\u042d\u0422\u041f\"",
          "supplier": "<b>\u0412\u043b\u0430\u0434\u0435\u043b\u0435\u0446 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u041e\u041e\u041e \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0418\u043d\u0436\u0438\u043d\u0438\u0440\u0438\u043d\u0433\"\n<b>\u0424\u0418\u041e:<\/b> \u041c\u043e\u0433\u0438\u043b\u044c\u043d\u044b\u0439 \u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d \u0412\u0438\u0442\u0430\u043b\u044c\u0435\u0432\u0438\u0447\n<b>\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c:<\/b> \u0413\u0435\u043d\u0435\u0440\u0430\u043b\u044c\u043d\u044b\u0439 \u0434\u0438\u0440\u0435\u043a\u0442\u043e\u0440\n<b>\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f:<\/b> \u041e\u041e\u041e \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0418\u043d\u0436\u0438\u043d\u0438\u0440\u0438\u043d\u0433\"\n<b>\u0418\u041d\u041d:<\/b> 7710946388\n<b>\u041a\u041f\u041f:<\/b> 770701001\n<b>\u0421\u0435\u0440\u0438\u0439\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440:<\/b> 01af4cdd005aab84844488b0f8375404be\n<b>\u0414\u0430\u0442\u0430 \u0432\u044b\u0434\u0430\u0447\u0438:<\/b> 07-02-2020 13:15:44 UTC\n<b>\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u0435\u043d \u0434\u043e:<\/b> 07-02-2021 13:25:44 UTC\n<b>\u0418\u0437\u0434\u0430\u0442\u0435\u043b\u044c \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u0410\u041e \"\u0415\u042d\u0422\u041f\""
        }
      }
    ],
    "currency": {
      "id": 810,
      "name": "RUB",
      "description": "\u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0438\u0439 \u0440\u0443\u0431\u043b\u044c",
      "groupid": "",
      "country_id": 142
    },
    "currentContract": {
      "id": 2245,
      "date_added": "2020-02-11T12:22:30+03:00",
      "is_last_version": true,
      "type_id": 1,
      "filename": "\u0414\u043e\u0433\u043e\u0432\u043e\u0440-917.rar",
      "hash": "a8c2d50faf764720b69f69063b2ea5e2ace16ca3ab4d5c6845296c5ec7b85818",
      "size": 102600,
      "supplier_id": 15,
      "customer_id": 4,
      "user_id": 4885,
      "info": false,
      "agreed": true,
      "date_agreed": "2020-02-11T12:22:30+03:00",
      "date_supplier_eds": "2020-02-12 17:08:25+03",
      "date_customer_eds": "2020-02-12 17:14:34+03",
      "oos_id": null,
      "oos_registry_number": null,
      "oos_version": 0,
      "oos_publish_status": 0,
      "name": "\u0414\u043e\u0433\u043e\u0432\u043e\u0440-917.rar",
      "sum_info": null,
      "term_info": null,
      "in_mongo": null,
      "actual": true,
      "awaiting_suppliers": false,
      "additional_suppliers_data": null,
      "registration_number": null,
      "parent_contract_id": null,
      "link": "\/file\/get\/t\/Contract\/id\/2245\/hash\/85f1d3f29b05a2cbb02e6d284b1c1ffc",
      "description": "",
      "descr": "",
      "date_agreed_end": "2020-02-18T23:59:00+03:00",
      "supplierSigned": true,
      "mainSupplierSigned": true,
      "customerSigned": true,
      "signatures": {
        "customer": "<b>\u0412\u043b\u0430\u0434\u0435\u043b\u0435\u0446 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u0413\u043e\u0441\u0443\u0434\u0430\u0440\u0441\u0442\u0432\u0435\u043d\u043d\u0430\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440\"\n<b>\u0424\u0418\u041e:<\/b> \u0421\u043e\u043b\u043e\u0432\u044c\u0435\u0432 \u0414\u043c\u0438\u0442\u0440\u0438\u0439 \u041d\u0438\u043a\u043e\u043b\u0430\u0435\u0432\u0438\u0447\n<b>\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c:<\/b> \u0414\u0438\u0440\u0435\u043a\u0442\u043e\u0440 \u0434\u0435\u043f\u0430\u0440\u0442\u0430\u043c\u0435\u043d\u0442\u0430 \u0437\u0435\u043c\u0435\u043b\u044c\u043d\u044b\u0445 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0439 \u0438 \u0443\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f \u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e\u043c\n<b>\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f:<\/b> \u0413\u043e\u0441\u0443\u0434\u0430\u0440\u0441\u0442\u0432\u0435\u043d\u043d\u0430\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440\"\n<b>\u0418\u041d\u041d:<\/b> 7717151380\n<b>\u041a\u041f\u041f:<\/b> 770701001\n<b>\u0421\u0435\u0440\u0438\u0439\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440:<\/b> 011ec68800f6aacab64a36a250f87a353a\n<b>\u0414\u0430\u0442\u0430 \u0432\u044b\u0434\u0430\u0447\u0438:<\/b> 30-10-2019 08:07:59 UTC\n<b>\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u0435\u043d \u0434\u043e:<\/b> 30-10-2020 08:17:59 UTC\n<b>\u0418\u0437\u0434\u0430\u0442\u0435\u043b\u044c \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u0410\u041e \"\u0415\u042d\u0422\u041f\"",
        "supplier": "<b>\u0412\u043b\u0430\u0434\u0435\u043b\u0435\u0446 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u041e\u041e\u041e \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0418\u043d\u0436\u0438\u043d\u0438\u0440\u0438\u043d\u0433\"\n<b>\u0424\u0418\u041e:<\/b> \u041c\u043e\u0433\u0438\u043b\u044c\u043d\u044b\u0439 \u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d \u0412\u0438\u0442\u0430\u043b\u044c\u0435\u0432\u0438\u0447\n<b>\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c:<\/b> \u0413\u0435\u043d\u0435\u0440\u0430\u043b\u044c\u043d\u044b\u0439 \u0434\u0438\u0440\u0435\u043a\u0442\u043e\u0440\n<b>\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f:<\/b> \u041e\u041e\u041e \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0418\u043d\u0436\u0438\u043d\u0438\u0440\u0438\u043d\u0433\"\n<b>\u0418\u041d\u041d:<\/b> 7710946388\n<b>\u041a\u041f\u041f:<\/b> 770701001\n<b>\u0421\u0435\u0440\u0438\u0439\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440:<\/b> 01af4cdd005aab84844488b0f8375404be\n<b>\u0414\u0430\u0442\u0430 \u0432\u044b\u0434\u0430\u0447\u0438:<\/b> 07-02-2020 13:15:44 UTC\n<b>\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u0435\u043d \u0434\u043e:<\/b> 07-02-2021 13:25:44 UTC\n<b>\u0418\u0437\u0434\u0430\u0442\u0435\u043b\u044c \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u0410\u041e \"\u0415\u042d\u0422\u041f\""
      },
      "oos_contract_info": [
        {
          "type": 1,
          "oos_contract_id": null,
          "version": null,
          "date_published": null,
          "status": null,
          "date_send": null,
          "error_detail": null
        },
        {
          "type": 2,
          "oos_contract_id": null,
          "version": null,
          "date_published": null,
          "status": null,
          "date_send": null,
          "error_detail": null
        }
      ],
      "send_to_oos": true
    },
    "agreementRequired": true,
    "signatureRequired": 1,
    "supplierRefusedSign": true,
    "contract_eds_text": "<b>\u0412\u043b\u0430\u0434\u0435\u043b\u0435\u0446 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u041e\u041e\u041e \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0418\u043d\u0436\u0438\u043d\u0438\u0440\u0438\u043d\u0433\"\n<b>\u0424\u0418\u041e:<\/b> \u041c\u043e\u0433\u0438\u043b\u044c\u043d\u044b\u0439 \u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d \u0412\u0438\u0442\u0430\u043b\u044c\u0435\u0432\u0438\u0447\n<b>\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c:<\/b> \u0413\u0435\u043d\u0435\u0440\u0430\u043b\u044c\u043d\u044b\u0439 \u0434\u0438\u0440\u0435\u043a\u0442\u043e\u0440\n<b>\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f:<\/b> \u041e\u041e\u041e \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0418\u043d\u0436\u0438\u043d\u0438\u0440\u0438\u043d\u0433\"\n<b>\u0418\u041d\u041d:<\/b> 7710946388\n<b>\u041a\u041f\u041f:<\/b> 770701001\n<b>\u0421\u0435\u0440\u0438\u0439\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440:<\/b> 01af4cdd005aab84844488b0f8375404be\n<b>\u0414\u0430\u0442\u0430 \u0432\u044b\u0434\u0430\u0447\u0438:<\/b> 07-02-2020 13:15:44 UTC\n<b>\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u0435\u043d \u0434\u043e:<\/b> 07-02-2021 13:25:44 UTC\n<b>\u0418\u0437\u0434\u0430\u0442\u0435\u043b\u044c \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u0410\u041e \"\u0415\u042d\u0422\u041f\"",
    "participants_eds_info": {
      "customer": "<b>\u0412\u043b\u0430\u0434\u0435\u043b\u0435\u0446 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u0413\u043e\u0441\u0443\u0434\u0430\u0440\u0441\u0442\u0432\u0435\u043d\u043d\u0430\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440\"\n<b>\u0424\u0418\u041e:<\/b> \u0421\u043e\u043b\u043e\u0432\u044c\u0435\u0432 \u0414\u043c\u0438\u0442\u0440\u0438\u0439 \u041d\u0438\u043a\u043e\u043b\u0430\u0435\u0432\u0438\u0447\n<b>\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c:<\/b> \u0414\u0438\u0440\u0435\u043a\u0442\u043e\u0440 \u0434\u0435\u043f\u0430\u0440\u0442\u0430\u043c\u0435\u043d\u0442\u0430 \u0437\u0435\u043c\u0435\u043b\u044c\u043d\u044b\u0445 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0439 \u0438 \u0443\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f \u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e\u043c\n<b>\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f:<\/b> \u0413\u043e\u0441\u0443\u0434\u0430\u0440\u0441\u0442\u0432\u0435\u043d\u043d\u0430\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440\"\n<b>\u0418\u041d\u041d:<\/b> 7717151380\n<b>\u041a\u041f\u041f:<\/b> 770701001\n<b>\u0421\u0435\u0440\u0438\u0439\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440:<\/b> 011ec68800f6aacab64a36a250f87a353a\n<b>\u0414\u0430\u0442\u0430 \u0432\u044b\u0434\u0430\u0447\u0438:<\/b> 30-10-2019 08:07:59 UTC\n<b>\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u0435\u043d \u0434\u043e:<\/b> 30-10-2020 08:17:59 UTC\n<b>\u0418\u0437\u0434\u0430\u0442\u0435\u043b\u044c \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u0410\u041e \"\u0415\u042d\u0422\u041f\"",
      "supplier": "<b>\u0412\u043b\u0430\u0434\u0435\u043b\u0435\u0446 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u041e\u041e\u041e \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0418\u043d\u0436\u0438\u043d\u0438\u0440\u0438\u043d\u0433\"\n<b>\u0424\u0418\u041e:<\/b> \u041c\u043e\u0433\u0438\u043b\u044c\u043d\u044b\u0439 \u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d \u0412\u0438\u0442\u0430\u043b\u044c\u0435\u0432\u0438\u0447\n<b>\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c:<\/b> \u0413\u0435\u043d\u0435\u0440\u0430\u043b\u044c\u043d\u044b\u0439 \u0434\u0438\u0440\u0435\u043a\u0442\u043e\u0440\n<b>\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f:<\/b> \u041e\u041e\u041e \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0418\u043d\u0436\u0438\u043d\u0438\u0440\u0438\u043d\u0433\"\n<b>\u0418\u041d\u041d:<\/b> 7710946388\n<b>\u041a\u041f\u041f:<\/b> 770701001\n<b>\u0421\u0435\u0440\u0438\u0439\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440:<\/b> 01af4cdd005aab84844488b0f8375404be\n<b>\u0414\u0430\u0442\u0430 \u0432\u044b\u0434\u0430\u0447\u0438:<\/b> 07-02-2020 13:15:44 UTC\n<b>\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u0435\u043d \u0434\u043e:<\/b> 07-02-2021 13:25:44 UTC\n<b>\u0418\u0437\u0434\u0430\u0442\u0435\u043b\u044c \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u0430:<\/b> \u0410\u041e \"\u0415\u042d\u0422\u041f\""
    },
    "additional_suppliers": false,
    "enough_money_for_sign": null,
    "contract_repudation": false
  }
}
```

