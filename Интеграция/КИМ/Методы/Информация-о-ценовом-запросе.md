## Описание метода
Возвращает информацию о запросах (входящих / исходящих)

## Запрос
Action: `Priceorder` 
Method: `getList`

* `data` 
    * `sort` - Поле сортировки 
    * `dir` - Направление сортировки (ASC/DESC) 
    * `contragent_type` - Тип запросов (1 - исходящие, как поставщика , 2 - входящие, как заказчика) 

### Пример запроса
```json
{
  "action": "Priceorder",
  "method": "getList",
  "data": [
    {
      "sort": "id",
      "dir": "DESC",
      "contragent_type": 2
    }
  ]
}
```

## Ответ
* `result` 
    * `rows` - Массив запросов 
        * `id` - ID запроса 
        * `type` - Тип запроса (id)
        * `title` - Наименование запроса
        * `customer_id` - Заказчик запроса
        * `user_id` - 
        * `date_created` - Дата создания
        * `date_sent` - Дата отправки
        * `date_response` - Дата ответа
        * `date_delivery` - Дата доставки
        * `status` - Статус запроса
        * `delivery_address` - Адрес доставки
        * `delivery_conditions` - Условия оплаты и доставки
        * `decision_basis` - Обоснование выбора поставщика 
        * `winner_supplier_id` - Победител
        * `reject_reason` - Причина отказа
        * `approve_reason` - Обоснование проведения закупки 
        * `actual` - Актуальность запроса
        * `type_name` - Тип (текстовое представление) 
        * `customer` - Заказчик (наименование)
        * `user_fio` - ФИО пользовател
        * `order_id` - ID заказа
        * `suppliers_count` - Кол-во привлеченных поставщиков
        * `responses_count` - Количество ответов
    
### Пример ответа
```json
{
  "action": "Priceorder",
  "method": "getList",
  "tid": 23,
  "type": "rpc",
  "result": {
    "main_module": "com",
    "rows": [
      {
        "id": 30,
        "type": "std",
        "title": null,
        "customer_id": 69,
        "user_id": 1598,
        "date_created": "2020-02-11 17:29:31+03",
        "date_sent": null,
        "date_response": null,
        "date_delivery": null,
        "status": 1,
        "delivery_address": null,
        "delivery_conditions": "\u041f\u043e\u0441\u0442\u043e\u043f\u043b\u0430\u0442\u0430 100% \u0432 \u0442\u0435\u0447\u0435\u043d\u0438\u0435 10 \u0440\u0430\u0431\u043e\u0447\u0438\u0445 \u0434\u043d\u0435\u0439",
        "decision_basis": null,
        "winner_supplier_id": null,
        "reject_reason": null,
        "approve_reason": null,
        "actual": true,
        "type_name": "\u0426\u0417",
        "customer": "\u0410\u041e \"\u0410\u0432\u0442\u043e\u0434\u043e\u0440-\u0422\u0435\u043b\u0435\u043a\u043e\u043c\"",
        "user_fio": "\u041a\u0443\u043d\u0438\u043d \u0411\u043e\u0440\u0438\u0441 \u041b\u044c\u0432\u043e\u0432\u0438\u0447",
        "order_id": "{}",
        "suppliers_count": 1,
        "responses_count": 0
      }
    ],
    "totalCount": 12,
    "success": true
  }
}
```

